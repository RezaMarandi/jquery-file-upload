<?php
include_once('config.php');

// define ChartNo
$stringParts = str_split(substr(date_timestamp_get(date_create()), 3, 7));
sort($stringParts);
$_SESSION['ChartNo'] = (1000000 + (implode('', $stringParts)));

if ($_GET['softwareName'] == '') {
  if ($_SESSION['softwareName'] == '') {
    redirect('/admin/select-software.php');
  }
} else {
  $_SESSION['softwareName'] = $_GET['softwareName'];
  redirect('/admin/select-service.php');
}

// Load services price records
$DB->ServicePriceRecord();

// Load order records
$DB->GetOrderRecourds(null, array(
  'intChartNo' => $_SESSION["ChartNo"],
  'intPatientID' => $_SESSION["PatientId"],
  'strSoftwareType' => $_SESSION["softwareName"],
  'intDoctorID' => $_SESSION['admin']["intDoctorID"],
));

// if ('hesam.bayat@gmail.com' === $_SESSION['admin']['strEmail']) {
  // echo '<pre style="padding: 2rem; margin: 1rem; border: solid 4px #ccc; font-size: \'Courier New\', Courier, sans-serif; font-size: 1rem; line-height: 1.5;">', var_dump($_SESSION), '</pre>';
// }

// Update Order

include_once('helpers/service-parameters.php');


if ($_POST) {

  $Msg = new Msg();

  $params = renderServiceParameters();

  // Order has beed added
  if ($DB->InsertServiceRequest($params)) {
    redirect('/admin/payment.php');
  } else {
    $Msg->collectMessage('Error! Please provide correct username and password', 'e');
    $_SESSION['msg'] = $Msg->getMsg();
  }
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
	<!--<![endif]-->
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<title>
			<?php echo PRE_PAGE_TITLE; ?>
		</title>
		<link href="styles/layout.css" rel="stylesheet" type="text/css">
		<link href="styles/reveal.css" rel="stylesheet">
		<link href="styles/fv-service-page.css" rel="stylesheet" type="text/css">
		<script src="js/jquery.min.js" type="text/javascript">
		</script>
		<script src="js/jquery.reveal.js" type="text/javascript">
		</script>
		<link href="styles/uploadfile.css" rel="stylesheet">
		<style type="text/css">
      .selectRequestCheckBox .alert {
        float: none;
        display: inline-block;
        width: auto;
      }
		          input[type=button] {
		              color:#08233E !important;
		              font:2.4em Futura, 'Century Gothic', AppleGothic, sans-serif;
		              font-size:70%;
		              padding:14px;
		              background:#ffcc00;
		              background-color:rgba(255,204,0,1);
		              border:1px solid #ffcc00;
		              -moz-border-radius:10px;
		              -webkit-border-radius:10px;
		              border-radius:10px;
		              border-bottom:1px solid #9f9f9f;
		              -moz-box-shadow:inset 0 1px 0 rgba(255,255,255,0.5);
		              -webkit-box-shadow:inset 0 1px 0 rgba(255,255,255,0.5);
		              box-shadow:inset 0 1px 0 rgba(255,255,255,0.5);
		              cursor:pointer;
		          }
		          input[type=button]:hover {
		              background-color:rgba(255,204,0,0.8);
		          }
		          .ajax-upload-dragdrop {
		              margin-left: 0px;
		          }
		          .ajax-file-upload-statusbar{
		              margin: 10px 0 0 0;
		          }
		          .ajax-file-upload-progress{
		              width:200px;
		          }
		          .test {
		              background-color: #E4685D;
		              border-radius: 4px;
		              box-shadow: 0 39px 0 -24px #E67A73 inset;
		              color: #FFFFFF;
		              cursor: pointer;
		              display: inline-block;
		              font-family: arial;
		              font-size: 13px;
		              font-weight: normal;
		              margin-right: 5px;
		              padding: 4px 15px;
		              text-decoration: none;
		              text-shadow: 0 1px 0 #B23E35;
		              vertical-align: top;
		          }
		          .optionBoxValidation .alert{
		              width: 150px;
		              margin: 16px 0 0 -150px !important;
		          }
		          .rqstfrom h2 {
		              font: bold 14px/18px Arial;
		          }
		          .rqstfrom h2 a{
		              text-decoration:none;
		              color:#d4ae27;
		          }
		          .rqstfrom h2 a:hover{
		              color:#e5bb28;
		          }
		          .acc_block1_header_price, .acc_block2_header_price, .acc_block3_header_price{
		              margin-left:5px;
		          }
		          #status_oth { color: #006600; }
		          .servicerequest{
		              right:-265px;
		          }
		          .servicerequest h2{
		              transform:rotate(270deg);
		              -moz-transform:rotate(270deg);
		              -o-transform:rotate(270deg);
		              -webkit-transform:rotate(270deg);
		              -ms-transform:rotate(270deg);
		              margin: -50px -30px 0px -270px;
		          }
		          .servicerequest p {
		              padding: 0 0 0 17px;
		          }
		</style>
		<script src="js/jquery.uploadfile.min.js"></script>
    <?php

    $store = array(
      'tariff' => $DB->ServicesFees,
      'cart' => $DB->OrderRecourds
    );

    include_once('parts/service-js.php');

    ?>
    <?php include_once(CONTENTS . 'main-menu-css-js.php'); ?>
	</head>
	<body>
		<?php include_once(CONTENTS . 'common-block.php'); ?>
		<div id="container">
			<div id="header">
				<?php include_once(CONTENTS . 'header.php'); ?><?php include_once(CONTENTS . 'top-menu.php'); ?><?php include_once(CONTENTS . 'common-search.php'); ?>
			</div>
			<div id="bgwrap">
				<div id="content">
					<div id="main">
						<h2 style="border-bottom:1px solid #e5eef5; font:normal 18px/22px Arial; color:#5E5E5E;">
							Request Service
						</h2><?php printMsg(); ?>
						<div class="rqstfrom">
							<h2>
								<a href="/admin/">Step 1: Select Patient</a> &mdash; <a href="/admin/select-software.php">Step 2: Select Software</a> &mdash; Step 3: Select Services
							</h2>
							<form action="" enctype="multipart/form-data" id="request_service" method="post" name="request_service" novalidate="">
								<input name="chart_no" type="hidden" value="<?php echo $_SESSION['ChartNo']; ?>">
								<div style="width:100%; clear:both"></div>

                <?php
                if ($_SESSION['softwareName'] == 'Simplant' || strtolower($_SESSION['softwareName']) == 'no software'):
                  include_once('parts/services/3d-conversion.php');
                else:
                  include_once('parts/services/files.php');
                endif;

                include_once('parts/services/treatment.php');
                include_once('parts/services/radiology.php');
                ?>

								<div class="request_service" style="margin-top:10px;">
									<p style="color:#D44C38;">
										Disclaimer:
									</p>
									<div class="item_tooltip" style="position:absolute; top:-4px; left:77px;">
										<div class="tooltip help">
											<span><img src="images/tool.png"></span>
											<div class='content'>
												<b></b>
												<p>
													Click desclaimer if not selected
												</p>
											</div>
										</div>
									</div>
								</div>
								<div style="width:100%; clear:both"></div>
								<p style="font-size:12px; vertical-align:top">
									<input id="disclaimer" name="disclaimer" type="checkbox" <?php echo "Y" == $DB->OrderRecourds['detail']['booDisclaimer'] ? 'checked' : ''; ?>> <input id="disclaimer_val" name="disclaimer_val" required="" type="hidden" value="1"> By clicking on "Submit Order" button below you are agreeing to the disclaimer <span class="view_disclaimer" style="color:#D44C38; font-weight:bold; font-size:12px; font-style:normal; cursor:pointer;">listed here</span>. <span>(Required)</span>
								</p>
								<div id="pop_up">
									<div class="reveal-modal" id="myModal2" style=" top:10% !important; height:400px; overflow:auto; border-radius:0px;">
										<p>
											Disclaimer ::
										</p>
										<p style="text-align:center;">
											<b>Consent for the Radiology Report</b>
										</p>
										<p>
											We highly recommend that the clinician request a full review of the imaging by our AAOMR Board certified radiologist.
										</p>
										<p>
											Our Board Certified Oral and Maxillofacial radiologists review the DICOM files that our clients have sent and offer a comprehensive report on each case. Reports will discuss the "whole" region that was imaged not just the area of interest and may include screenshots and measurements per the clinicians request, ADDITIONAL CHARGE MAY APPLY. Our maxillofacial radiologist is available all the time to answer any questions. Please feel free to contact us CinZara, LLC E-mail: info@CinZara, LLC.
										</p>
										<p>
											If you opt not to have the DICOM/imaging reviewed by our on board oral and maxillofacial radiologist, then, you are fully responsible to interpret and provide a report by yourself or somebody of your choice to read and interpret your patient scan.
										</p>
										<p>
											Should you omit to perform such control or decide to nevertheless not to use the radiology report, you frees CinZara, LLC from any liability for the consequences of the presence of dental and other pathologies Finally, it is the clinician responsibility to read the report details and discuss it with radiologist if he/she disagree with the Oral and maxillofacial radiologist opinion.
										</p>
										<p>
											Management<br>
											CinZara, LLC
										</p><a class="close-reveal-modal">&#215;</a>
									</div>
								</div>
								<div style="width:100%; clear:both"></div>
								<div class="selectRequestCheckBox item bad" style="display:none;">
									<div class="alert">Please click on the checkbox</div>
								</div>
								<div style="width:100%; clear:both"></div>
                <?php if ($DB->strCouponCode) { ?>
                <p style="color:#D44C38;">Coupone:</p>
                <p class="coupon_code"><?php echo $DB->strCouponCode; ?> <span style="font: inherit;" class="discount-calc"></span></p>
                <input id="discount" name="discount" type="hidden" value="<?php echo $DB->strCouponCode; ?>">
                <?php } ?>
                <div style="width:100%; clear:both"></div>
								<p style="color:#D44C38;">Total:</p>
								<p id="total_price">$0.00</p>
                <input id="cart_total_price" name="cart_total_price" type="hidden" value="">
                <input id="cart_original_price" name="cart_original_price" type="hidden" value="">
								<p id="btns-actions" style="opacity: 0.2; pointer-events: none;">
									<a class="go_back" href="select-software.php"><button class="myButton_small go_back">Go Back</button></a>
                  <button class="myButton_small" id="submit_order" type="submit">Submit Order</button>
								</p>
							</form>
						</div>
					</div>
				</div>
				<div id="sidebar">
					<?php include_once(CONTENTS . 'left-bar.php'); ?>
				</div>
			</div>
		</div><?php include_once(CONTENTS . 'footer.php'); ?>
	</body>
</html>
